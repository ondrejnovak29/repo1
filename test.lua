local elements = {
    [8695] = {
        air = false,
        ammo = {
            [1] = {
                count = 45,
                defName = "rifle",
            },
            [2] = {
                count = 1,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 5984,
        teamID = 3,
    },
    [23] = {
        air = false,
        ammo = {
            [1] = {
                count = 25,
                defName = "mg",
            }
        },
        defName = "machinegunner",
        distance = 350,
        teamID = 1,    
    },
    [2454] = {
        air = true,
        ammo = {
            [1] = {
                count = 1140,
                defName = "aircannon",
            }
        },
        defName = "fighter",
        distance = 1840,
        teamID = 2,    
    },
    [13541] = {
        air = false,
        ammo = {
            [1] = {
                count = 15,
                defName = "rifle",
            },
            [2] = {
                count = 3,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 5321,
        teamID = 3,
    },
    [13544] = {
        air = false,
        ammo = {
            [1] = {
                count = 25,
                defName = "rifle",
            },
            [2] = {
                count = 2,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 5328,
        teamID = 3,
    },
    [14584] = {
        air = false,
        ammo = {
            [1] = {
                count = 45,
                defName = "rifle",
            },
            [2] = {
                count = 1,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 5228,
        teamID = 3,
    },
    [19854] = {
        air = false,
        ammo = {
            [1] = {
                count = 70,
                defName = "rifle",
            },
            [2] = {
                count = 6,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 5541,
        teamID = 3,
    },
    [52411] = {
        air = false,
        ammo = {
            [1] = {
                count = 7,
                defName = "HE",
            },
            [2] = {
                count = 15,
                defName = "sabot",
            },
        },
        defName = "tank",
        distance = 684,
        teamID = 1,
    },
    [58411] = {
        air = false,
        ammo = {
            [1] = {
                count = 5,
                defName = "HE",
            },
            [2] = {
                count = 8,
                defName = "sabot",
            },
        },
        defName = "tank",
        distance = 1001,
        teamID = 1,
    },
    [98741] = {
        air = false,
        ammo = {
            [1] = {
                count = 17,
                defName = "HE",
            },
            [2] = {
                count = 0,
                defName = "sabot",
            },
        },
        defName = "tank",
        distance = 1021,
        teamID = 2,
    },
    [22248] = {
        air = false,
        ammo = {
            [1] = {
                count = 18,
                defName = "rifle",
            },
            [2] = {
                count = 4,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 284,
        teamID = 1,
    },
    [85471] = {
        air = false,
        ammo = {
            [1] = {
                count = 0,
                defName = "rifle",
            },
            [2] = {
                count = 2,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 284,
        teamID = 1,
    },
    [95450] = {
        air = false,
        ammo = {
            [1] = {
                count = 0,
                defName = "rifle",
            },
            [2] = {
                count = 0,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 400,
        teamID = 1,
    },
    [95520] = {
        air = false,
        ammo = {
            [1] = {
                count = 0,
                defName = "rifle",
            },
            [2] = {
                count = 5,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 421,
        teamID = 1,
    },
    [89562] = {
        air = false,
        ammo = {
            [1] = {
                count = 0,
                defName = "rifle",
            },
            [2] = {
                count = 5,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 500,
        teamID = 1,
    },
    [84515] = {
        air = false,
        ammo = {},
        defName = "bugBuilder",
        distance = 1080,
        teamID = 4,
    },
    [98741] = {
        air = false,
        ammo = {
            [1] = {
                count = math.huge,
                defName = "acid",
            }
        },
        defName = "bugSpitter",
        distance = 1080,
        teamID = 4,
    },
}

local inserts = {
    [14751] = {
        air = false,
        ammo = {
            [1] = {
                count = 15,
                defName = "HE",
            },
            [2] = {
                count = 14,
                defName = "sabot",
            },
        },
        defName = "tank",
        distance = 2685,
        teamID = 1,
    },
    [95520] = {
        air = false,
        ammo = {
            [1] = {
                count = 14,
                defName = "HE",
            },
            [2] = {
                count = 14,
                defName = "sabot",
            },
        },
        defName = "tank",
        distance = 552,
        teamID = 1,
    },
    [84564] = {
        air = false,
        ammo = {},
        defName = "bugBuilder",
        distance = 1154,
        teamID = 4,
    },
    [84585] = {
        air = false,
        ammo = {},
        defName = "bugBuilder",
        distance = 1150,
        teamID = 4,
    },
    [55854] = {
        air = false,
        ammo = {
            [1] = {
                count = 58,
                defName = "rifle",
            },
            [2] = {
                count = 3,
                defName = "grenade",
            },
        },
        defName = "soldier",
        distance = 400,
        teamID = 2,
    },
    [11474] = {
        air = true,
        ammo = {
            [1] = {
                count = 1300,
                defName = "aircannon",
            }
        },
        defName = "fighter",
        distance = 10585,
        teamID = 2,    
    },
}

local deletionsByID = {8695, 84585, 84515}



print('Practice Loops')

N = 7
for i = 1, N do
    print(i)
end
print("\n")
for i = 2, N, 2 do
    print(i)
end
print("\n")
for i = N, 1, -1 do
    print(i)
end

print("\n\n\nPractice Arrays")

a = { 5, 3, 6, 1, 6, 10, 12 }
sum = 0
for _, v in ipairs(a) do
    sum = sum + v
end
print(sum .. "\n")
maxIdx, maxV = nil, nil
for i, v in ipairs(a) do
    maxV = maxV or v
    if v >= maxV then
        maxIdx = i
        maxV     = v
    end
end
print((maxIdx or "nil") .. "\n")
squares = { }
for i = 1, N do
    squares[i] = i * i
end
print(table.concat(squares, ", ") .. "\n")
nth = 2
for _, v in ipairs(a) do
    if v % 2 == 0 then nth = nth - 1 end

    if nth == 0 then
        print(v)
        break
    end
end

print("\n\n\nPractice arrays vs. tables")

for i, v in ipairs(a) do
    print(i .. ": " .. v)
end
print("\n")
t = { first_name = "Ondra", last_name = { has = "Yes", whatIsIt = "Kappa" } }
for k, v in pairs(t) do
    if type(v) ~= "table" then
        print(k .. ": " .. v)
    end
end
print("\n")
print(#a .. "\n")
count = 0
for _ in pairs(t) do
    count = count + 1
end
print(count .. "\n")
print(#a == 0)
print("\n")
print(next(t) == nil)

print("\n\n\nPractice tables in basic applications")

function spaces(n)
    return string.rep(" ", n)
end
function print_t(tab, indent)
    indent = indent or 0
    if type(tab) ~= "table" then
        print(spaces(indent) .. tab)
        return
    end
    for k, v in pairs(tab) do
        print(spaces(indent) .. k .. ": ")
        print_t(v, indent + 4)
    end
end
print_t(t)
print("\n")
function filter(array, predicate)
    local new_array = {}
    for _, v in ipairs(array) do
        if predicate(v) then
            new_array[#new_array + 1] = v
        end
    end
    return new_array
end
print(table.concat(filter(a, function(v) return v % 2 == 0 end), ", ") .. "\n")


print("\n\n\nComplex table practice")

for k, _ in pairs(elements) do io.write(k .. " ") end print("\n")
for k, v in pairs(inserts) do
    elements[k] = v
end
for k, _ in pairs(elements) do io.write(k .. " ") end print("\n")
for _, id in ipairs(deletionsByID) do
    elements[id] = nil
end
for k, _ in pairs(elements) do io.write(k .. " ") end print("\n")

print("air")
air = {}
for k, v in pairs(elements) do
    if v["air"] then air[k] = v end
end
for k, _ in pairs(air) do io.write(k .. " ") end print("\n")

print("tanks")
tank = {}
for k, v in pairs(elements) do
    if v["defName"] == "tank" then tank[k] = v end
end
for k, _ in pairs(tank) do io.write(k .. " ") end print("\n")

print("tanks ammo")
function teamUnits(t, id)
    local units = {}
    for k, v in pairs(t) do
        if v["teamID"] == id then units[k] = v end
    end
    return pairs(units)
end
ammo = {}
for k, v in teamUnits(elements, 1) do
    if v["defName"] == "tank" then ammo[k] = v["ammo"] end
end
for v, _ in pairs(ammo) do io.write(v .. " ") end print("\n")

print("units with ammo")
some_weapon = {}
for k, v in teamUnits(elements, 4) do
    if next(v["ammo"]) then some_weapon[k] = v end
end
for k, _ in pairs(some_weapon) do io.write(k .. " ") end print("\n")

print("enemies near")
enemies_near = {}
for k, v in pairs(elements) do
    if v["distance"] <= 1500 and v["teamID"] ~= 1 then enemies_near[k] = v end
end
for k, _ in pairs(enemies_near) do io.write(k .. " ") end print("\n")

print("grenade count")
function grenadeCount(ammo)
    for _, v in pairs(ammo) do
        if v["defName"] == "grenade" then return v["count"] end
    end
    return 0
end
for i = 1, 4 do
    print("TEAM " .. i)
    local soldiers = { }
    for k, v in teamUnits(elements, i) do
        print("    " .. k .. ": " .. grenadeCount(v["ammo"]))
        if v["defName"] == "soldier" then
            soldiers[k] = v
        end
    end

    print("   soldiers:")
        for k, v in pairs(soldiers) do
        print("    " .. k .. ": " .. grenadeCount(v["ammo"]))
    end
end
print()

print("can kill 100?")
KP1 = { rifle = 0, machinegun = 0, acid = 0 }
KP10 = { HE = 0 }
for i = 1, 4 do
    local kill_power = 0
    for k, v in teamUnits(elements, i) do
        for _, a in pairs(v["ammo"]) do
            if KP1 [a["defName"]] then kill_power = kill_power +  1 * a["count"] end
            if KP10[a["defName"]] then kill_power = kill_power + 10 * a["count"] end
        end
    end
    print("TEAM " .. i .. ": " .. (kill_power >= 100 and "YES" or "NO"))
end